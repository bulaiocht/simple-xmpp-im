package com.simple.messenger.xmpp;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;


@Controller
public class XMPPManager {

    @Autowired
    private AbstractXMPPConnection getXMPPConnection;

    @RequestMapping(path ="/")
    public String showChat(){
        return "show-chat";
    }


    /**
     * Closing the current connection to server
     */
    @RequestMapping(path = "/disconnect", method = RequestMethod.POST)
    public String disconnect(){
        if (getXMPPConnection != null && getXMPPConnection.isConnected()){
            getXMPPConnection.disconnect();
        }
        return "show-chat";
    }

    /**
     * Setting presence status
     * @param available
     * @param status
     */
    public void setStatus (boolean available, String status){
        Presence presence;
        Presence.Type presenceType;

        if (available){
            presenceType = Presence.Type.available;
        } else {
            presenceType = Presence.Type.unavailable;
        }

        presence = new Presence(presenceType);
        presence.setStatus(status);
    }

    /**
     * Sending message to a specific JID
     * @throws XmppStringprepException
     * @throws SmackException.NotConnectedException
     * @throws InterruptedException
     */
    @RequestMapping(path = "/send-message", method = RequestMethod.POST)
    public String sendMessage (HttpServletRequest request) throws XmppStringprepException, SmackException.NotConnectedException, InterruptedException {
        String message = request.getParameter("message");
        ChatManager chatManager = ChatManager.getInstanceFor(getXMPPConnection);
        EntityBareJid bareJid = (EntityBareJid) JidCreate.from(request.getParameter("buddyJID"));
        Chat chat = chatManager.chatWith(bareJid);
        Message xmppMessage = new Message();
        xmppMessage.setBody(message);
        chat.send(xmppMessage);
        return "show-chat";
    }

    /**
     * It's a listener for incoming messages
     * it won't work unless it's running in some kind of container, so commented
     * everything needs to be put in Spring MVC environment I guess
     */
    /*chatManager.addIncomingListener((from, incomingMessage, currentChat) ->
            System.out.printf("From %1$s: %2$s",from, incomingMessage.getBody()));*/

}
