package com.simple.messenger.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.simple.messenger")
@Import(SimpleIMConnectionConfig.class)
public class SimpleIMSpringConfig {

}
