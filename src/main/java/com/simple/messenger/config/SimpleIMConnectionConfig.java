package com.simple.messenger.config;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.bosh.BOSHConfiguration;
import org.jivesoftware.smack.bosh.XMPPBOSHConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.IOException;

@Configuration
@PropertySource("classpath:simple-im-connection.properties")
public class SimpleIMConnectionConfig {
    
    @Autowired
    private Environment environment;
    
    @Bean
    public AbstractXMPPConnection getXMPPConnection () throws IOException, InterruptedException, XMPPException, SmackException {

        /*BOSHConfiguration.Builder configBuilder = BOSHConfiguration.builder();
        configBuilder
                .setUsernameAndPassword("bot", "bot")
                .setFile("/http-bind/")
                .setPort(5280)
                .setXmppDomain("localhost")
                .setHost("yajim.com");
        XMPPBOSHConnection connection = new XMPPBOSHConnection(configBuilder.build());*/

        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        configBuilder.setUsernameAndPassword(environment.getProperty("username"),environment.getProperty("password"))
        .setDebuggerEnabled(true)
        .setHost(environment.getProperty("host"))
        .setPort(Integer.parseInt(environment.getProperty("port")))
        .setXmppDomain(environment.getProperty("domain"))
        .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        XMPPTCPConnection connection = new XMPPTCPConnection(configBuilder.build());

        connection.connect();
        connection.login();

        return connection;
    }
}
